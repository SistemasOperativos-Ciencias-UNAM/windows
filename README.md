# Windows

## Comparación con UNIX

Functional Comparison of UNIX and Windows - Microsoft TechNet
+ <https://technet.microsoft.com/en-us/library/bb496993.aspx>

## Proceso de inicio de Windows NT

The Windows 7 Boot Process (sbsl) - Microsoft TechNet
+ <https://social.technet.microsoft.com/wiki/contents/articles/11341.the-windows-7-boot-process-sbsl.aspx>

Secure the Windows 8.1 boot process - Microsoft TechNet
+ <https://technet.microsoft.com/en-us/windows/dn168167.aspx>

Windows booting procedure - Acronis
+ <https://kb.acronis.com/sites/default/files/content/2006/01/1934/windows_booting_procedure.pdf>

## Arquitectura de Windows

Windows Architecture – The Basics
+ <https://blogs.technet.microsoft.com/askperf/2007/04/10/windows-architecture-the-basics/>

Windows NT 4.0 Architecture (archived)
+ <https://technet.microsoft.com/en-us/library/cc749980.aspx>

Architecture of Windows 10
+ <https://social.technet.microsoft.com/wiki/contents/articles/31048.architecture-of-windows-10.aspx>

Deeper into Windows Architecture - Microsoft Developer
+ <https://blogs.msdn.microsoft.com/hanybarakat/2007/02/25/deeper-into-windows-architecture/>

Architecture of the Windows Kernel - Microsoft
Department of Computer Science - Florida State University
+ <http://www.cs.fsu.edu/~zwang/files/cop4610/Fall2016/windows.pdf>

Windows Architecture (archived) - Microsoft TechNet
+ <https://technet.microsoft.com/en-us/library/cc768129.aspx>

Active Directory and Windows 2000 Architecture (archived)
+ <https://technet.microsoft.com/en-us/library/cc961759.aspx>

Windows NT 4.0 Workstation Architecture (archived)
+ <https://technet.microsoft.com/en-us/library/cc749980.aspx>

### User-mode y Kernel-mode

User mode and kernel mode - Microsoft Hardware Dev Center
+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/gettingstarted/user-mode-and-kernel-mode>

Windows Architecture and User/Kernel Mode - Reverse Engineering - InfoSec Institute
+ <http://resources.infosecinstitute.com/windows-architecture-and-userkernel-mode/>

Kernel-Mode Driver Architecture Design Guide
+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/kernel/>

Network Architecture for Kernel-Mode Drivers
+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/network/network-architecture-for-kernel-mode-drivers>

## Kernel

Inside the Windows Vista Kernel
+ <https://technet.microsoft.com/en-us/library/cc748650.aspx>

## Drivers

Overview of the Windows I/O Model
+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/kernel/overview-of-the-windows-i-o-model>

Kernel Anti Virus Filters Testing Overview
+ <https://msdn.microsoft.com/en-us/library/gg607475.aspx>

Windows 2000 and Windows XP Networking Design Guide
+ <https://msdn.microsoft.com/en-us/library/windows/hardware/ff565849.aspx>

Network Drivers, Windows Vista and Later
+ <https://msdn.microsoft.com/en-us/library/windows/hardware/ff571081.aspx>

### Plug and play

How Plug and Play Works
+ <https://technet.microsoft.com/en-us/library/cc781092.aspx>

### Firmado de drivers

Cross-Certificates for Kernel Mode Code Signing
+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/install/cross-certificates-for-kernel-mode-code-signing>

Windows Driver Signing Tutorial
+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/install/windows-driver-signing-tutorial>o

### NDIS - Network Driver Interface Specification

Kernel debugging over the network - The NDIS Blog - Microsoft MSDN
+ <https://blogs.msdn.microsoft.com/ndis/2014/03/10/kernel-debugging-over-the-network/>

Overview of Remote NDIS (RNDIS) - Microsoft Hardware Dev Center
+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/network/overview-of-remote-ndis--rndis->

NDIS driver types
+ <https://docs.microsoft.com/en-us/windows-hardware/drivers/network/ndis-drivers>

--------------------------------------------------------------------------------

## Herramientas

Microsoft Systems Journal - Microsoft
+ <https://www.microsoft.com/msj/>
+ <https://www.microsoft.com/msj/code.aspx>

Windows Internals Book
+ <https://docs.microsoft.com/en-us/sysinternals/learn/windows-internals>
+ <https://github.com/zodiacon/WindowsInternals>
